#ifndef DRAW_H
#define DRAW_H
#include "model.h"
#define SKYBOX_SIZE 10000.0

typedef struct Position
{
	double x;
	double y;
	double z;
}Position;

typedef struct {

	double planet_rotation;
	double sun_rotation;
}Rotate;

typedef struct{
	Position planet;
	Position sun;
	Position star;
}Move;


void draw_model(const struct Model* model);


void draw_triangles(const struct Model* model);


void draw_quads(const struct Model* model);


void draw_normals(const struct Model* model, double length);


void draw_skybox_top(Entity skybox);


void draw_skybox_bottom(Entity skybox);


void draw_environment(World world,Rotate* rotate, Move move);


#endif
