#include "camera.h"
#include "draw.h"
#include <GL/glut.h>
#include <math.h>
#define M_PI 3.14159265358979323846
#define skybox_size 6000
#define sun_size 850
#define planet_size 200
#define moon_size 85
#define star_size 150


double degree_to_radian(double degree)
{
	return degree * M_PI / 180.0;
}



void init_camera(struct Camera* camera)
{
	camera->position.x = 3000;
	camera->position.y = 0;
	camera->position.z = 0;

	camera->pose.x = 0;
	camera->pose.y = 0;
	camera->pose.z = 180;
}


void don_not_head_up_against_the_wall (struct Camera* camera, Move move){


	if (camera->position.x<-skybox_size || camera->position.x>skybox_size ||
		camera->position.y<-skybox_size || camera->position.y>skybox_size ||
		camera->position.z<-skybox_size || camera->position.z>skybox_size)
		init_camera (camera);

	if (camera->position.x<sun_size && camera->position.x>-sun_size &&
			camera->position.y<sun_size && camera->position.y>-sun_size &&
			camera->position.z<sun_size && camera->position.z>-sun_size)
			init_camera (camera);

	if (camera->position.x < move.planet.x+planet_size && camera->position.x>move.planet.x-planet_size &&
			camera->position.y<move.planet.y+planet_size && camera->position.y>move.planet.y-planet_size &&
			camera->position.z<move.planet.z+planet_size && camera->position.z>move.planet.z-planet_size)
			init_camera (camera);

	if (camera->position.x < move.planet.x+1000+moon_size && camera->position.x>move.planet.x+1000-moon_size &&
			camera->position.y<move.planet.y+1000+moon_size && camera->position.y>move.planet.y+1000-moon_size &&
			camera->position.z<move.planet.z-100+moon_size && camera->position.z>move.planet.z-100-moon_size)
			init_camera (camera);

	if (camera->position.x < move.star.x +star_size+1500 && camera->position.x>move.star.x-star_size &&
			camera->position.y<move.star.y+star_size && camera->position.y>move.star.y-star_size &&
			camera->position.z<move.star.z+star_size && camera->position.z>move.star.z-star_size)
			init_camera (camera);

}


void set_view_point(const struct Camera* camera)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glRotatef(-(camera->pose.x + 90), 1.0, 0, 0);
	glRotatef(-(camera->pose.z - 90), 0, 0, 1.0);
	glTranslatef(-camera->position.x, -camera->position.y, -camera->position.z);
}



void rotate_camera(struct Camera* camera, double horizontal, double vertical)
{
	camera->pose.z += horizontal/CAMERA_SPEED;
	camera->pose.x += vertical/CAMERA_SPEED;

	if (camera->pose.z < 0) {
		camera->pose.z += 360.0;
	}

	if (camera->pose.z > 360.0) {
		camera->pose.z -= 360.0;
	}

	if (camera->pose.x < 0) {
		camera->pose.x += 360.0;
	}

	if (camera->pose.x > 360.0) {
		camera->pose.x -= 360.0;
	}

}


void move_camera_up(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	camera->position.z += distance;

}



void move_camera_down(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	camera->position.z -= distance;
}



void move_camera_backward(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z);

	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;


}


void move_camera_forward(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z);


	camera->position.x += cos(angle) * distance;
	camera->position.y += sin(angle) * distance;

}


void step_camera_right(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z + 90.0);


	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;

}


void step_camera_left(struct Camera* camera, double distance)
{
	camera->prev_position = camera->position;
	double angle = degree_to_radian(camera->pose.z - 90.0);

	camera->position.x -= cos(angle) * distance;
	camera->position.y -= sin(angle) * distance;

}
